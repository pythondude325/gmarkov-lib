# gmarkov-lib
![pipeline](https://gitlab.com/pythondude325/gmarkov-lib/badges/master/pipeline.svg)

This library provides the `MarkovChain` data structure. The markov chains are
fed by many finite sequence of items and outputs a sequence to try and imitate
the training data. In my implementation, the input must be an Iterator where
item is `Eq + Hash + Clone` (or its alias `ChainItem`).

Most commonly this library is used for sequences of `char`s, but can be used for
almost any datatype, like numbers or enums.

For more information see the documentation: [https://docs.rs/gmarkov-lib](https://docs.rs/gmarkov-lib)
