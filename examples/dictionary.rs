extern crate gmarkov_lib;

use std::fs::File;
use std::io::{Result, BufRead, BufReader};
use gmarkov_lib::MarkovChain;

fn main() -> Result<()> {
    let mut chain = MarkovChain::with_order::<char>(2);

    let reader = BufReader::new(File::open("dictionary_sample.txt")?);

    for line in reader.lines() {
        chain.feed(line?.chars());
    }

    println!("New word: {}", chain.into_iter().collect::<String>());

    Ok(())
}
