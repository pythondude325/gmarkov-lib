use std::collections::HashMap;
use super::ChainItem;

#[derive(Clone, Debug)]
pub struct WeightedSet<I: ChainItem> {
    inner: HashMap<I, u32>,
    total_weight: u32,
}

impl<I: ChainItem> WeightedSet<I> {
    pub fn new() -> WeightedSet<I> {
        WeightedSet {
            inner: HashMap::new(),
            total_weight: 0,
        }
    }

    pub fn count(&mut self, item: &I) {
        *self.inner.entry(item.clone()).or_default() += 1;
        self.total_weight += 1
    }

    pub fn choose(&self) -> Option<&I> {
        let mut number = (rand::random::<u32>() % self.total_weight) as i32;

        for (item, weight) in &self.inner {
            number -= *weight as i32;
            if number < 0 {
                return Some(item);
            }
        }

        None
    }
}

impl<I: ChainItem> Default for WeightedSet<I> {
    fn default() -> Self {
        WeightedSet::new()
    }
}
